DROP TABLE IF EXISTS `members`;

CREATE TABLE `members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ae_id` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `address` text,
  `city` varchar(255) DEFAULT NULL,
  `province` smallint(6) DEFAULT NULL,
  `zip` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `bb_pin` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `email_2` varchar(255) DEFAULT NULL,
  `email_3` varchar(255) DEFAULT NULL,
  `activation_code` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Data for the table `members` */

insert  into `members`(`id`,`ae_id`,`code`,`name`,`address`,`city`,`province`,`zip`,`phone`,`bb_pin`,`email`,`email_2`,`email_3`,`activation_code`,`password`,`status`) values (9,NULL,'160009','Aswin Syirojul Huda Lubis','asdf','safsdf',16,'asdfsf','sdfsf','sadfsf','awin.poenye@gmail.com','','','9733e403eaa3c9990b72384f3a8f67a0','aaa',1),(10,NULL,'050010','Awin Syirojul','skjfksj','jsfksj',5,'54545','asfsfjkjk','sakksj','awin.email.system@gmail.com','','','5024b5cb5c21078599d8b0d4608fac0e','aaa',1),(11,NULL,'110011','noraz','Jl. Teluk Mandar Ujung No.99 Kompleks Angkatan Laut,\nRawa Bambu, Pasar Minggu Jakarta Selatan 12520','jakarta',11,'12520','12345678910','','noor.aziz13@gmail.com','','','87747aa480bcece287ef268960c474a1','noraz',1);

/*Table structure for table `order_items` */

DROP TABLE IF EXISTS `order_items`;

CREATE TABLE `order_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `paket_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '1',
  `amount` decimal(10,0) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8;

/*Data for the table `order_items` */

insert  into `order_items`(`id`,`order_id`,`paket_id`,`quantity`,`amount`) values (58,9,2,2,'1950000'),(59,10,1,1,'650000'),(60,10,3,3,'3900000'),(61,11,2,2,'1950000'),(62,12,5,6,'12000000'),(63,13,2,1,'975000'),(64,14,2,1,'975000'),(65,15,5,6,'12000000'),(66,16,5,6,'12000000'),(67,17,5,6,'12000000'),(68,18,2,3,'2925000'),(69,19,3,2,'2600000'),(70,20,3,2,'2600000'),(71,21,3,2,'2600000'),(72,22,1,1,'650000'),(73,23,1,1,'650000'),(74,24,1,1,'650000'),(75,25,1,1,'650000'),(76,26,1,1,'650000'),(77,27,1,1,'650000'),(78,28,1,2,'1300000'),(79,29,1,1,'650000'),(80,29,3,1,'1300000'),(81,30,5,2,'4000000');

/*Table structure for table `orders` */

DROP TABLE IF EXISTS `orders`;

CREATE TABLE `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `member_id` int(11) NOT NULL,
  `total_amount` decimal(11,0) NOT NULL,
  `shipping_cost` decimal(9,2) NOT NULL DEFAULT '0.00',
  `total_price` decimal(9,2) NOT NULL DEFAULT '0.00',
  `notes` varchar(1024) DEFAULT NULL,
  `status` smallint(6) DEFAULT '0',
  `created_dt` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `payment_dt` datetime DEFAULT NULL,
  `delivery_dt` datetime DEFAULT NULL,
  `awb_number` varchar(255) NOT NULL,
  `shipping_receiver` varchar(255) DEFAULT NULL,
  `shipping_address` text,
  `shipping_city` varchar(255) DEFAULT NULL,
  `shipping_province` varchar(255) DEFAULT NULL,
  `shipping_zip` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

/*Data for the table `orders` */

insert  into `orders`(`id`,`code`,`member_id`,`total_amount`,`shipping_cost`,`total_price`,`notes`,`status`,`created_dt`,`payment_dt`,`delivery_dt`,`awb_number`,`shipping_receiver`,`shipping_address`,`shipping_city`,`shipping_province`,`shipping_zip`) values (9,NULL,0,'0','0.00','0.00',NULL,0,'2014-06-06 00:12:15',NULL,NULL,'','Aswin Syirojul Huda Lubis','asdf','safsdf','16','asdfsf'),(10,NULL,9,'4550000','0.00','4550000.00',NULL,0,'2014-06-06 00:16:37',NULL,NULL,'','Aswin Syirojul Huda Lubis','asdf','safsdf','16','asdfsf'),(11,'201406-160009-11',9,'1950000','0.00','1950000.00',NULL,0,'2014-06-06 00:23:39',NULL,NULL,'','Aswin Syirojul Huda Lubis','asdf','safsdf','16','asdfsf'),(12,'160009-12',9,'12000000','0.00','9999999.99',NULL,0,'2014-06-06 00:24:25',NULL,NULL,'','Aswin Syirojul Huda Lubis','asdf','safsdf','16','asdfsf'),(13,'160009-13',9,'975000','0.00','975000.00',NULL,0,'2014-06-06 00:31:54',NULL,NULL,'','Aswin Syirojul Huda Lubis','asdf','safsdf','16','asdfsf'),(14,'160009-14',9,'975000','0.00','975000.00',NULL,0,'2014-06-06 00:32:25',NULL,NULL,'','Aswin Syirojul Huda Lubis','asdf','safsdf','16','asdfsf'),(15,'160009-15',9,'12000000','0.00','9999999.99',NULL,0,'2014-06-06 00:33:42',NULL,NULL,'','Aswin Syirojul Huda Lubis','asdf','safsdf','16','asdfsf'),(16,'160009-16',9,'12000000','0.00','9999999.99',NULL,0,'2014-06-06 00:34:34',NULL,NULL,'','Aswin Syirojul Huda Lubis','asdf','safsdf','16','asdfsf'),(17,'160009-17',9,'12000000','0.00','9999999.99',NULL,0,'2014-06-06 00:34:44',NULL,NULL,'','Aswin Syirojul Huda Lubis','asdf','safsdf','16','asdfsf'),(18,'160009-18',9,'2925000','0.00','2925000.00',NULL,0,'2014-06-06 00:35:35',NULL,NULL,'','Aswin Syirojul Huda Lubis','asdf','safsdf','16','asdfsf'),(19,'160009-19',9,'2600000','0.00','2600000.00',NULL,0,'2014-06-06 00:36:57',NULL,NULL,'','Aswin Syirojul Huda Lubis','asdf','safsdf','16','asdfsf'),(20,'160009-20',9,'2600000','0.00','2600000.00',NULL,0,'2014-06-06 00:37:41',NULL,NULL,'','Aswin Syirojul Huda Lubis','asdf','safsdf','16','asdfsf'),(21,'160009-21',9,'2600000','0.00','2600000.00',NULL,0,'2014-06-06 00:37:55',NULL,NULL,'','Aswin Syirojul Huda Lubis','asdf','safsdf','16','asdfsf'),(22,'160009-22',9,'650000','0.00','650000.00',NULL,0,'2014-06-06 00:38:08',NULL,NULL,'','Aswin Syirojul Huda Lubis','asdf','safsdf','16','asdfsf'),(23,'160009-23',9,'650000','0.00','650000.00',NULL,0,'2014-06-06 00:39:38',NULL,NULL,'','Aswin Syirojul Huda Lubis','asdf','safsdf','16','asdfsf'),(24,'160009-24',9,'650000','0.00','650000.00',NULL,0,'2014-06-06 00:39:57',NULL,NULL,'','Aswin Syirojul Huda Lubis','asdf','safsdf','16','asdfsf'),(25,'160009-25',9,'650000','0.00','650000.00',NULL,0,'2014-06-06 00:41:03',NULL,NULL,'','Aswin Syirojul Huda Lubis','asdf','safsdf','16','asdfsf'),(26,'160009-26',9,'650000','0.00','650000.00',NULL,0,'2014-06-06 00:41:42',NULL,NULL,'','Aswin Syirojul Huda Lubis','asdf','safsdf','16','asdfsf'),(27,'160009-27',9,'650000','0.00','650000.00',NULL,1,'2014-06-06 00:42:29',NULL,NULL,'','Aswin Syirojul Huda Lubis','asdf','safsdf','16','asdfsf'),(28,'050010-28',10,'1300000','0.00','1300000.00',NULL,0,'2014-06-05 20:07:21',NULL,NULL,'','Awin Syirojul','skjfksj','jsfksj','5','54545'),(29,'050010-29',10,'1950000','0.00','1950000.00',NULL,0,'2014-06-05 20:07:59',NULL,NULL,'','Awin Syirojul','skjfksj','jsfksj','5','54545'),(30,'110011-30',11,'4000000','0.00','4000000.00',NULL,0,'2014-06-06 03:11:45',NULL,NULL,'','noraz','Jl. Teluk Mandar Ujung No.99 Kompleks Angkatan Laut,\nRawa Bambu, Pasar Minggu Jakarta Selatan 12520','jakarta','11','12520');

/*Table structure for table `paket` */

DROP TABLE IF EXISTS `paket`;

CREATE TABLE `paket` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `short_description` varchar(255) DEFAULT NULL,
  `description` text,
  `quantity` smallint(6) DEFAULT NULL,
  `price` decimal(9,0) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

/*Data for the table `paket` */

insert  into `paket`(`id`,`product_id`,`name`,`short_description`,`description`,`quantity`,`price`) values (1,1,'Ency Nabi 2 Paket','2 Paket Buku Myfirst Islamic Enclopebee @12pcs',NULL,2,'650000'),(2,1,'Ency Nabi 3 Paket','3 Paket Buku Myfirst Islamic Enclopebee @12pcs',NULL,3,'975000'),(3,1,'Ency Nabi 4 Paket','4 Paket Buku Myfirst Islamic Enclopebee @12pcs',NULL,4,'1300000'),(4,1,'Ency Nabi 5 Paket','5 Paket Buku Myfirst Islamic Enclopebee @12pcs',NULL,5,'1625000'),(5,1,'Ency Nabi 7 Paket','7 Paket Buku Myfirst Islamic Enclopebee @12pcs',NULL,7,'2000000');

/*Table structure for table `payments` */

DROP TABLE IF EXISTS `payments`;

CREATE TABLE `payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `confirm_date` int(11) DEFAULT NULL,
  `checked_date` int(11) DEFAULT NULL,
  `transfer_date` int(11) DEFAULT NULL,
  `account_number` varchar(255) DEFAULT NULL,
  `account_bank` varchar(255) DEFAULT NULL,
  `account_name` varchar(255) DEFAULT NULL,
  `destination_account` varchar(255) DEFAULT NULL,
  `transfer_amount` decimal(9,0) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `payments` */

insert  into `payments`(`id`,`order_id`,`confirm_date`,`checked_date`,`transfer_date`,`account_number`,`account_bank`,`account_name`,`destination_account`,`transfer_amount`) values (5,27,1402419632,NULL,2014,'578454878','BRI','Saya sendiri','BCA-WarungBuncit No. 552 038 9204','500000');

/*Table structure for table `product` */

DROP TABLE IF EXISTS `product`;

CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `unit` varchar(255) DEFAULT NULL,
  `price` decimal(9,0) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `product` */

insert  into `product`(`id`,`name`,`unit`,`price`) values (1,'My First Islamic Encyclopebee','Pcs','97000');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `district` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `zip` varchar(255) DEFAULT NULL,
  `province` varchar(255) DEFAULT NULL,
  `telp` varchar(255) DEFAULT NULL,
  `bb_pin` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `user` */

insert  into `user`(`id`,`role`,`code`,`name`,`address`,`district`,`city`,`zip`,`province`,`telp`,`bb_pin`,`email`) values (1,'admin',NULL,'Aswin S','asdkjskfj','kjsdfkj','jakarta','15454','DKI JAKARTA','45454','454','awin.poenye@gmail.com');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
