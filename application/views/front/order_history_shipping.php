	<div class="form">
		<label class="legend" >ITEM PAKET</label>
	</div>
	<div class="p">
		<table border="1" cellspacing="0" style="border-collapse: collapse;" >
			<thead>
				<th style="width: 30px">No.</th>
				<th style="width: 100px">Nomor DO</th>
				<th style="width: 150px">Paket</th>
				<th style="width: 300px">Tujuan Kirim</th>
				<th style="width: 70px">Jumlah Kirim</th>
				<th style="width: 120px">Status</th>
				<th style="width: 150px">AWB NUMBER</th>
			</thead>
			<tbody>
				<?php
				$row = 0;
				$order = $order[0];
				foreach ($shipping as $value) { 
					$row++  ?>
					<tr>
						<td style="text-align: center" ><?= $row ?></td>
						<td style="text-align: center" ><?= $order->code . '-' . $value->do_number ?></td>
						<td style="text-align: left;padding-left: 5px">
							<?= $order->name ?>
						</td>
						<td style="padding-left: 5px">
							<?= $value->shipping_receiver ?>
							<br />
							<?= $value->shipping_address ?>
							<br />
							<?= $value->shipping_city . ' ' . provinceTeks($value->shipping_province) . ' ' . $value->shipping_zip ?>
							<br />
							Telp. <?= $value->shipping_phone ?>
						</td>
						<td style="text-align: center"><?= $value->shipping_qty ?></td>
						<td style="text-align: center"><?= shippingStatusTeks($value->status) ?></td>
						<td style="text-align: center"><?= $value->awb_number ?></td>
					</tr>
					<?php
				} ?>
				
			</tbody>
		</table>	

	</div>
	
		