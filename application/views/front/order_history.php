<br />
	<div style="width: 960px;margin: auto;" >
		<div class="frame" style="text-align: center;font-size: 18px; font-weight: bold">HISTORI ORDER</div>
		
		<div class="frame top" style=""  >
			<div id="history-nav">
				<ul>
					<li rel="list" class="selected" >LIST ORDER</li>
					<li rel="detail" style="display: none" >DETAIL</li>
					<li rel="shipping" style="display: none" >PENGIRIMAN</li>
				</ul>
			</div>
		</div>
		<div  class="frame bottom"  >
			<div id="history-list" style="background-color: white;padding: 10px;">

				<table class="table" id="table_order">

						<thead >
						<tr>
							<td colspan="5" style="text-align: right;color: red;padding-right: 10px;background-color: white; border: none " >* Klik pada baris data untuk melihat detail</td>
						</tr>
						</thead>
						<thead >
						<tr class="top">
							<td class="left" style="width: 50px;">No</td>
							<td style="width: 150px;" >TANGGAL</td>
							<td style="width: 120px;" >KODE ORDER</td>
							<td style="width: 150px;" >TOTAL HARGA</td>
							<td style="width: 200px;" >STATUS</td>

						</tr>
					</thead>
					<tbody>
						<?php
						$row = 0;
						foreach ($order as $key => $value) { 
							$row++;
							?>
							<tr class="order" rel="<?= $value->id ?>" >
								<td class="left"><?= $row ?></td>
								<td style="text-align: left " ><?= date('d F Y H:i',strtotime( $value->created_dt)) ?></td>
								<td style="text-align: left " ><?= $value->code ?></td>
								<td style="text-align: right " ><?= number_format($value->total_price,0) ?></td>
								<td class="status_<?= $value->status ?>" ><?= saleStatusTeks( $value->status) ?></td>
							</tr>
							<?php
						} ?>
					</tbody>
				</table>
			</div>
 			<div id="history-box" style="background-color: white;padding: 10px;display: none">

 			</div>
		</div>
	</div>


		