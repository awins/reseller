<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>RESELLER PUSLEB</title>
      	<link rel="stylesheet" href="<?= base_url('public/css/front.css') ?>" type="text/css" media="screen">
      	<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
		
	</head>
	<?php 
		$member = isLogged();

	?>

	<body>
		<div id="bg-header" >
			<header id="container-nav">
            
            <div id="login" >
                <?php
                    
                if ($member != false) { ?>
                    <div style="font-size: 18px;padding-right: 10px">
                        <div style="text-align: right;padding-right: 25px" >
                            <span style="color: white" >Selamat datang, </span><span style="color: gold;text-shadow: 2px 2px black" ><?= $member->name ?></span>
                        </div>
                        <div id="account" >
                            <ul>
                                <li><a href="<?= base_url('member/logout') ?>">Logout >></a></li>
                                <li><a href="<?= base_url('order/orderHistory') ?>">Histori Order >></a></li>
                                <li><a href="<?= base_url('member/profile') ?>">Akun Saya >></a></li>
                            </ul>
                        </div>
                    </div>
                    <?php
                } else { ?>
                    <form action="<?= base_url('member/login') ?>" method="post" style="float:right;" >
                        <label style="font-size:24px; margin-right:10px; color:#FFFFFF;">Login: </label>
                        <input type="text" name="email" placeholder="Email" value="" id="email-input" />
                        <input type="password" name="password" placeholder="Password" value="" id="pass-input" />
                        <input type="submit"  value="" style="display: none" />
                    </form>
                    <p style="clear:both;"></p>
                    <div id="account" >
                            <ul>
                                <li><a href="<?= base_url('member/register') ?>">Register >></a></li>
                                <li><a href="<?= base_url('member/forgetPassword') ?>">Forget Password >></a></li>
                            </ul>
                    </div>
                    <?php
                } ?>
                
            <p style="clear:both;"></p>
                
            </div>

            
				<div id="navigation">
					<div></div>
					<div id="main-nav" >
                    	<div id="logo-wibee">
                        	<img src="../../../public/img/logo-wibee.jpg" width="80" height="80" />
                        </div>
						<div id="title">
							<span style="color:#C14800;">RESELLER</span> <br /> PUSTAKA LEBAH
						</div>
						<div id="menu">
							<ul>
								<?php
								if ($member) { ?>
									<li class="<?= (@$menu == 'order') ? 'selected' : NULL ?>" ><a href="<?= base_url('order') ?>">ORDER</a></li>
									<?php
								} ?>
								<li><a href="#">ABOUT</a></li>
								<li><a href="#">PRODUCTS</a></li>
								<li class="<?= ( @$menu == 'home' ) ? 'selected' : NULL ?>"><a href="<?= base_url() ?>">HOME</a></li>
							</ul>
						</div>
                        <p style="clear:both;"></p>
					</div>
				</div>
			</header>
            <div id="image-banner"><img src="../../../public/img/image-banner-2.png" /></div>
         </div>   
		
        <section id="page" >
        	<div id="deskripsi">
                <table width="960" border="1" style="border-collapse:collapse;">
                  <tr>
                    <td width="320" align="center"><span id="sub-judul">Apa Itu <br /> Reseller Pustaka Lebah?</span></td>
                    <td width="320" align="center"><span id="sub-judul">Apa Manfaat <br /> 
                    Reseller Pustaka Lebah?</td>
                    <td width="320" align="center"><span id="sub-judul">Bagaimana <br /> 
                    Caranya?</td>
                  </tr>
                  <tr>
                    <td align="center"><img src="../../../public/img/logo-reseller.png" width="180"  /></td>
                    <td align="center"><img src="../../../public/img/profit.png" width="240"  /></td>
                    <td align="center">&nbsp;</td>
                  </tr>
                  <tr>
                    <td align="center"><a href="#link-page"><button id="btn-des-reseller">Baca</button></a></td>
                    <td align="center"><a href="#link-page"><button id="btn-manfaat-reseller">Baca</button></a></td>
                    <td align="center"><a href="#link-page"><button id="btn-cara-reseller">Baca</button></a></td>
                  </tr>
                </table>
            </div>
            
            <div id="deskripsi-reseller">
            	satu
            </div>
            
            <div id="manfaat-reseller">
            	dua
            </div>
            
            <div id="cara-reseller">
            	tiga
            </div>
            
            <div id="link-page">
            </div>