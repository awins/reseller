	<div class="form">
		<label class="legend" >ITEM PAKET</label>
	</div>
	<div class="p">
		<table border="1" cellspacing="0" style="border-collapse: collapse;" >
			<thead>
				<th style="width: 30px">No.</th>
				<th style="width: 150px">Nama Paket</th>
				<th style="width: 300px">Deskripsi</th>
				<th style="width: 100px">Harga/Paket</th>
				<th style="width: 100px">Jumlah Pesan</th>
				<th style="width: 100px">Total Harga</th>
			</thead>
			<tbody>
				<?php
				$row = 0;
				foreach ($order as $value) { 
					$row++  ?>
					<tr>
						<td style="text-align: center" ><?= $row ?></td>
						<td style="text-align: left;padding-left: 5px"><?= $value->name ?></td>
						<td style="text-align: left;padding-left: 5px"><?= $value->short_description ?></td>
						<td style="text-align: right;padding-right: 5px"><?= number_format($value->price,0) ?></td>
						<td style="text-align: right;padding-right: 5px"><?= $value->quantity ?></td>
						<td style="text-align: right;padding-right: 5px"><?= number_format($value->amount,0) ?></td>
					</tr>
					<?php
				} ?>
				<tr style="height: 30px;font-weight: bold" class="no-border" >
					<td colspan="5" style="text-align: right;padding-right: 10px" >Total Harga Paket</td>
					<td style="text-align: right;padding-right: 5px" ><?= number_format($value->total_amount,0) ?></td>
				</tr>
				<tr style="height: 30px;font-weight: bold" class="no-border" >
					<td colspan="5" style="text-align: right;padding-right: 10px" >Total Ongkos Kirim</td>
					<td style="text-align: right;padding-right: 5px"  ><?= number_format($value->total_cost,0) ?></td>
				</tr>
				<tr style="height: 30px;font-weight: bold" class="no-border" >
					<td colspan="5" style="text-align: right;padding-right: 10px" >Total Tagihan</td>
					<td style="text-align: right;padding-right: 5px" ><?= number_format($value->total_price,0) ?></td>
				</tr>
			</tbody>
		</table>	

	</div>
	<form class="form" id="form_confirmation" method="post" action="" >
		<input type="hidden" name="submit_confirm" value="1" />
		<input type="hidden" name="total_price" value="<?= $value->total_price ?>" />

		<div class="p">
			<label class="legend" >PEMBAYARAN:</label>
		
			<?php
		    $payment = $order[0];
			
		    if ($post) {
		    	$transfer_date = explode('-', set_value('transfer_date'));
		    } else {
		        $transfer_date = time();
		        $transfer_date = array( date('Y', $transfer_date ), date('m', $transfer_date ), date('d', $transfer_date ) );
		    }
		    
			?>
			<input type="text" style="text-align: center" class="text w200 status_<?= $payment->status ?>" readonly value="<?= saleStatusTeks($payment->status) ?>" ?>
		</div>
		<div class="p" >
 			<label class="legend" >Data Pembayaran</label>

		</div>
		<div class="p">
 			<label class="title" >TGL TRANSFER</label>
			<select class="select-date" name="transfer_date[2]">
                <?php
                for ($i=1; $i <= 31 ; $i++) { ?>
                    <option <?= ($transfer_date[2] == $i) ? 'selected' : NULL ; ?> value="<?= sprintf('%02d', $i) ?>" > <?= $i ?> </option>
                    <?php
                } ?>

            </select>
            &nbsp; - &nbsp;
            

            <select class="select-date" name="transfer_date[1]">
                <?
                for ($i=1; $i <= 12 ; $i++) { ?>
                    <option <?= ($transfer_date[1] == $i) ? 'selected' : NULL ; ?> value="<?= sprintf('%02d', $i) ?>" > <?= $i ?> </option>
                    <?php
                } ?>

            </select>
            &nbsp; - &nbsp;
            <select class="select-date" name="transfer_date[0]">
                <?php
                for ($i=2014; $i <= 2030 ; $i++) { ?>
                    <option <?= ($transfer_date[0] == $i) ? 'selected' : NULL ; ?> value="<?= sprintf('%02d', $i) ?>" > <?= $i ?> </option>
                    <?php
                } ?>

            </select>
		</div>
		<div class="p" >
 			<label class="title" >Nomor Rek.</label>
 			<input type="text" class="text w250" name="account_number" value="<?= ($post) ? set_value('account_number') : $payment->account_number ?>" />
			<?= form_error('account_number') ?>

		</div>
		<div class="p" >
 			<label class="title" >Bank</label>
 			<input type="text" class="text w100" name="account_bank" value="<?= ($post) ? set_value('account_bank') : $payment->account_bank ?>" />
			<?= form_error('account_bank') ?>

		</div>
		<div class="p" >
 			<label class="title" >Nama Pemilik Rek.</label>
 			<input type="text" class="text w250" name="account_name" value="<?= ($post) ? set_value('account_name') : $payment->account_name ?>" />
			<?= form_error('account_name') ?>

		</div>
		<div class="p" >
 			<label class="title" >Rek. Bank Tujuan</label>
 			<select name="destination_account" >
 				<option value="" >-- PILIH --</option>
 				<?php
 				if ($post) {
 					$dest = set_value('destination_account');
 				} else {
 					$dest = $payment->destination_account;
 				}

 				foreach ($_REKENING as $value) { ?>
	 				<option <?= ($dest == $value ) ? 'selected' : NULL ?> value="<?= $value ?>" ><?= $value ?></option>
 					<?php
 				} ?>
 			</select>
			<?= form_error('destination_account') ?>
		</div>
		<div class="p" >
 			<label class="title" >Jumlah Transfer</label>
 			Rp. <input type="text" class="text w100 " style="text-align: right" id="transfer_amount" name="transfer_amount" value="<?= ($post) ? number_format(set_value('transfer_amount'),0) : 0 ?>" />
 				
			<?= form_error('transfer_amount') ?>

		</div>
		<?php 
		if ($payment->status == 0) { ?>
			<div class="p" >
	 			<label class="title" >&nbsp;</label>
	 			<input type="button" class="button" id="submit_confirm" value="KIRIM KONFIRMASI" />
			</div>
			<?php
		} ?>
	</form>
		