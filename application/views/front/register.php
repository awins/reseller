<br />
<form class="form" style="width: 860px;margin: auto" action="<?= base_url('member/register') ?>" method="post"  >
	<div class="frame" style="text-align: center;font-size: 18px; font-weight: bold">FORMULIR PENDAFTARAN RESELLER</div>
	<div class="frame" >

		<input type="hidden" name="register" value="1" />

		<div class="p">
			<label class="title" >Kode AE</label>
			<input name="ae_id" type="text" class="text w100" value="<?= set_value('ae_id') ?>" />
			<?= form_error('ae_id') ?>

		</div>

		<div class="p">
			<label class="title" >Nama Lengkap</label>
			<input name="name" type="text" class="text w250" value="<?= set_value('name') ?>" />
			<?= form_error('name') ?>

		</div>

		<div class="p">
			<label class="title" >Alamat</label>
			<textarea name="address" class="w350 h100" ><?= set_value('address') ?></textarea>
			<?= form_error('address') ?>

		</div>

		<div class="p">
			<label class="title" >Kab/Kota</label>
			<input name="city" type="text" class="text" value="<?= set_value('city') ?>" />
			<label  class="title" style="margin: 0 5px 0 20px; float: none; width: 150px" >Provinsi</label>
			<select name="province" >
				<option value="" >-- PILIH --</option>
				<?php
				for ($i=1; $i <= 33 ; $i++) { ?>
					<option value="<?= $i ?>" <?= set_select('province', $i) ?> ><?= provinceTeks($i) ?></option>
					<?php
				} ?>
			</select>
			<label class="title" style="margin: 0 5px 0 20px; float: none; width: 150px" >Kode Pos</label>
			<input name="zip" type="text" class="text" value="<?= set_value('zip') ?>" />
			<?= form_error('city') ?>
			<?= form_error('province') ?>
			<?= form_error('zip') ?>


		</div>

		<div class="p">
			<label class="title" >Telp/HP</label>
			<input name="phone" type="text" class="text w450" value="<?= set_value('phone') ?>"  />
			<label class="title" style="margin: 0 5px 0 20px; float: none; width: 150px" >Pin BB</label>
			<input name="bb_pin" type="text" class="text" value="<?= set_value('bb_pin') ?>" />
			<?= form_error('phone') ?>

		</div>
		<div class="p">
			<label class="title" >Email</label>
			<input name="email" type="text" class="text " value="<?= set_value('email') ?>" />
			<label class="title" style="margin: 0 5px 0 20px ; float: none; width: 150px" >Email-2</label>
			<input name="email_2" type="text" class="text " value="<?= set_value('email_2') ?>" />
			<label class="title" style="margin: 0 5px 0 20px; float: none; width: 150px" >Email-3</label>
			<input name="email_3" type="text" class="text " value="<?= set_value('email_3') ?>" />
			<?= form_error('email') ?>

		</div>
		<div class="p">
			<label class="title" >Password</label>
			<input name="password" type="password" class="text " value="" />
			<?= form_error('password') ?>
		</div>
		<div class="p" >
			<label class="title" >&nbsp;</label>
			<input type="checkbox" name="have_read" id="have_read" />
			<label for="have_read" >Saya telah membaca dan menyetujui syarat, ketentuan dan kebijakan privasi</label>
			<?php if (form_error('have_read') != NULL ) {?> 
				<div class="error_note">
					* syarat, ketentuan dan kebijakan privasi harus disetujui 
				</div>
				<?php
			} ?>

		</div>
		<div class="p">
			<label class="title" >&nbsp;</label>
			<label class="lbl_disclaimer" rel="term" style="margin-right: 40px" >Baca Syarat dan Ketentuan</label>
			<label class="lbl_disclaimer" rel="privacy">Baca Kebijakan Privasi</label>
		</div>
		<p style="text-align: center" >
			<input class="button" type="submit" value="DAFTAR" >
		</p>
	</div>
</form>

