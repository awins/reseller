			</section>
			<footer></footer>
		</div>
		<div id="disclaimer-term" title="Syarat dan ketentuan" style="display: none">
		 	<strong>Syarat</strong> <br />
		 	(1). Warga Negara Indonesia <br />
		 	(2). Berusia lebih dari 17 tahun atau sudah menikah <br />
		 	(3). Berdomisili di wilayah Indonesia <br /> <br />
		 	<strong>Ketentuan</strong> <br />
		 	(1). RESELLER bertanggungjawab penuh terhadap pelayanan Pelanggan/Customer-nya <br />
			(2). RESELLER diwajibkan menjalin komunikasi dan kerja sama yang positif dengan antarsesama Penyalur Resmi lainnya dalam melakukan pengawasan dan pemantauan kegiatan pemasaran produk.       <br />
			<br /> <br />

			Syarat dan ketentuan ini dapat juga dilihat di <a href="www.reseller.pustaka-lebah.com/home/termandcondisiton">disini</a>
		</div>

		<div id="disclaimer-privacy" title="Kebijakan Privasi" style="display: none">
		 	
			Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
			<br /><br /><br /><br />
			Kebijakan Privasi ini dapat juga dilihat di <a href="www.reseller.pustaka-lebah.com/home/termandcondisiton">disini</a>
		</div>
		<script type="text/javascript" src="<?= base_url('public/js/jquery.js') ?>"></script>
		<script type="text/javascript" src="<?= base_url('public/js/jquery-ui.min.js') ?>"></script>
		<script type="text/javascript" src="<?= base_url('public/js/jquery.form.js') ?>"></script>
		<script type="text/javascript" src="<?= base_url('public/js/front.js?15') ?>"></script>

	</body>
</html>

		