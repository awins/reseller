<br />
<form class="form" style="width: 960px;margin: auto" action="<?= base_url('order') ?>" method="post"  >
	<input type="hidden" name="order" value="1" />
	<input type="hidden" id="shipping_count" name="shipping_count" value="<?= $shipping_count ?>"  >
	<div class="frame" style="text-align: center;font-size: 18px; font-weight: bold;">FORMULIR PEMESANAN</div>

	<div class="frame" >
		<div class="p">
			<label class="legend" >Reseller</label>
		</div>
		<div class="p">
			<label class="title" >Kode :</label>
			<input readonly type="text" class="text w150" value="<?= $member->code  ?>" />
		</div>
		<div class="p">
			<label class="title" >Nama :</label>
			<input readonly type="text" class="text w350" value="<?=  $member->name ?>" />
		</div>
	</div>
	<div class="frame" >
		<div class="p">
			<label class="legend" >Item Paket</label>
		</div>
		<div style="color: red;margin-left: 50px">
				* Minimal pembelian untuk paket reseller Ency Nabi adalah 2 paket
				<br />
				* Discount Rp. 275.000 untuk pembelian 7 paket dan kelipatannya
			</div>
		<div class="p">
			<table border="1" cellspacing="0" style="border-collapse: collapse;margin:auto" >
				<thead>
					<th style="width: 30px">No.</th>
					<th style="width: 150px">Nama Paket</th>
					<th style="width: 300px">Deskripsi</th>
					<th style="width: 100px">Harga/Paket</th>
					<th style="width: 50px">Jumlah</th>
					<th style="width: 80px">Discount</th>
					<th style="width: 80px">Total Harga</th>

				</thead>
				<tbody>
					<?php
					$row = 0;
					foreach ($paketColl as $key => $value) { 
						$row++  ?>
						<tr>
							<td style="text-align: center" ><?= $row ?></td>
							<td style="text-align: left;padding-left: 5px"><?= $value->name ?></td>
							<td style="text-align: left;padding-left: 5px"><?= $value->short_description ?></td>
							<td style="text-align: right;padding-right: 5px"><?= number_format($value->price,0) ?></td>
							<td style="text-align: right;padding-right: 5px">
								<select  id="quantity_<?= $value->id ?>" name="quantity[<?= $value->id ?>]" class="text w50 input-number quantity" >
									<?php
									for ($i=2; $i < 49 ; $i++) {  
										if ($i % 6 == 0) continue;
										?>
										<option <?= ($post && $_POST['quantity'][$value->id] == $i) ? 'selected' : false  ?> ><?= $i ?></option>
										<?
									} ?>
								</select>
							</td>
							<td style="text-align: right;padding-right: 5px">
								<input type="text" id="discount_<?= $value->id ?>" name="discount[<?= $value->id ?>]" readonly style="text-align: right;width: 85px" value="<?= ($post) ? $_POST['discount'][$value->id] : NULL ?>" >
							</td>
							<td style="text-align: right;padding-right: 5px">
								<input type="text" id="amount_<?= $value->id ?>" name="amount[<?= $value->id ?>]" readonly style="text-align: right;width: 85px" class="amount"  value="<?= ($post) ? $_POST['amount'][$value->id] : NULL ?>" >
							</td>
						</tr>
						<?php
					} ?>
					
					<tr style="height: 30px;font-weight: bold" class="no-border" >
						<td colspan="6" style="text-align: right;padding-right: 10px" >Total Harga Paket</a></td>
						<td style="text-align: right;padding-right: 5px" id="total_amount" >0</td>
					</tr>
					<tr style="height: 30px;font-weight: bold" class="no-border" >
						<td colspan="6" style="text-align: right;padding-right: 10px" >Total Ongkos Kirim</a></td>
						<td style="text-align: right;padding-right: 5px" id="total_cost" >0</td>
					</tr>
					<tr style="height: 30px;font-weight: bold" class="no-border" >
						<td colspan="6" style="text-align: right;padding-right: 10px" >Total</a></td>
						<td style="text-align: right;padding-right: 5px" id="total_price" >0</td>
					</tr>
				</tbody>
			</table>	
			<?= form_error('paket') ?>
			<br />
			<?= (form_error('shipping')) ? '<div class="error_note">* Jumlah paket harus sama dengan jumlah pengiriman</div>' : NULL ?>

		</div>
	</div>
	<div id="shipping_box" >
		<?php
		foreach ($shipping_form as$value) {
			echo $value;
		} ?>
	</div>
	<div class="frame" style="" >
		<input id="btnAddShipping" class="button" type="button" value="TAMBAH ALAMAT PENGIRIMAN" >
	</div>

	<div class="frame" style="text-align: left" >
		<div class="p" >
			<input type="checkbox" name="have_read" id="have_read" />
			<label for="have_read" >Saya telah membaca dan menyetujui syarat, ketentuan dan kebijakan privasi</label>
			<?php if (form_error('have_read') != NULL ) {?> 
				<div class="error_note" style="margin-left: 20px" >
					* syarat, ketentuan dan kebijakan privasi harus disetujui 
				</div>
				<?php
			} ?>

		</div>
		<div class="p">
			<label class="lbl_disclaimer" rel="term" style="margin-right: 40px" >Baca Syarat dan Ketentuan</label>
			<label class="lbl_disclaimer" rel="privacy">Baca Kebijakan Privasi</label>
		</div>
		<div style="text-align: center"> 
			<input class="button" type="submit" value="PESAN SEKARANG" >
		</div>
	</div>
</form>
	<br />
	<br />
	<br />
	<br />