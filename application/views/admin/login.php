<html>
	<head>
	<title>RESELLER ADMIN LOGIN</title>
      <link rel="stylesheet" href="<?= base_url('public/css/admin.css') ?>" type="text/css" media="screen">
	</head>
	<body >

		<div id="login-box"  >
			<div id="login-title">
				ADMINISTRATOR LOGIN
			</div>
			<form style="margin-bottom: 0" method="post" action="<?= site_url( 'admin/login') ?>" >
				<div id="login-body" >
						<input type="hidden" name="login" value="1" />
						<div >	
							<label style="width:70px;display: inline-block;font-weight: bold"  >User</label>
							<input type="text" id="login-user" name="user_name"  class="w200" autocomplete="off" /> 
						</div>

						<div style="margin-top: 10px" >	
							<label style="width:70px;display: inline-block;font-weight: bold"  >Password</label>
							<input type="password" name="user_pass" class="w200" autocomplete="off" /> 
						</div>
					</div>
					<div id="login-footer"  >
						<div style="float: left;color: red" >
							<? 
							if (isset($tpl['status']))  { ?>
								Wrong user Name and or Password
								<?
							} ?>
						</div>
						<div style="float: right;width:80px;text-align: right" >
							<input id="submit" type="submit" name="submit" value="Login"  class="button-green p1" /> 
						</div>					
					</div>
			</form>
		</div>
		<script type="text/javascript" src="<?= site_url('public/js/jquery.js') ?>"></script>
			
		<script>
	      $(document).ready(function(){   
				$('#login-user').focus();
	    	
	        });    
		</script>
		
	</body>
</html>