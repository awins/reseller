<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Service extends CI_Controller {

	public function index()
	{
		$this->load->view('front/header');
		$this->load->view('front/footer');
	}

	function getOrderDetail($order_id) {
		$this->load->model('order_model','Order');
		$this->load->model('member_model','Member');
		$this->load->model('shipping_model','Shipping');

		$res = $this->Order->get_detail_by_id($order_id);

		$arr['data'] = $res;
		$arr['rowCount'] = count($res);
		if (count($res) > 0) {
			$arr['colCount'] = count((array) $res[0]);
			$customer = $this->Member->get_by_id($res[0]->member_id);
			$ae = $this->Member->get_by_id($customer->ae_id);
			$arr['customer'] = $customer;
			$arr['ae'] = $ae;

			$arr['shipping'] = $this->Shipping->get_by_order($order_id);

		} else {
			$arr['colCount'] = 0;
		}

		echo json_encode($arr);
	}

	function provinceSQL($field) {
		return "(CASE " . $field . "
		WHEN 1 THEN 'Nanggro Aceh Darussalam'
		WHEN 2 THEN 'Sumatera Utara'
		WHEN 3 THEN 'Sumatera Barat'
		WHEN 4 THEN 'Riau'
		WHEN 5 THEN 'Kepulauan Riau'
		WHEN 6 THEN 'Jambi'
		WHEN 7 THEN 'Sumatera Selatan'
		WHEN 8 THEN 'Kep. Bangka Belitung'
		WHEN 9 THEN 'Bengkulu'
		WHEN 10 THEN 'Lampung'
		WHEN 11 THEN 'DKI Jakarta'
		WHEN 12 THEN 'Jawa Barat'
		WHEN 13 THEN 'Banten'
		WHEN 14 THEN 'Jawa Tengah'
		WHEN 15 THEN 'DI Yogyakarta'
		WHEN 16 THEN 'Jawa Timur'
		WHEN 17 THEN 'Bali'
		WHEN 18 THEN 'Nusa Tenggara Barat'
		WHEN 19 THEN 'Nusa Tenggara Timur'
		WHEN 20 THEN 'Kalimantan Barat'
		WHEN 21 THEN 'Kalimantan Tengah'
		WHEN 22 THEN 'Kalimantan Selatan'
		WHEN 23 THEN 'Kalimantan Timur'
		WHEN 24 THEN 'Kalimantan Utara'
		WHEN 25 THEN 'Sulawesi Utara'
		WHEN 26 THEN 'Sulawesi Barat'
		WHEN 27 THEN 'Sulawesi Tengah'
		WHEN 28 THEN 'Sulawesi Tenggara'
		WHEN 29 THEN 'Sulawesi Selatan'
		WHEN 30 THEN 'Gorontalo'
		WHEN 31 THEN 'Maluku'
		WHEN 32 THEN 'Maluku Utara'
		WHEN 33 THEN 'Papua'
		WHEN 34 THEN 'Papua Barat'
		ELSE '' END) ";
	}

	function saleStatusSQL($field) {
		return "(CASE " . $field . "
		WHEN 0 THEN 'TUNGGU KONFIRMASI'
		WHEN 1 THEN 'CEK KOMFIRMASI'
		WHEN 2 THEN 'KONFIRMASI DISETUJUI'
		WHEN 3 THEN 'KONFIRMASI DITOLAK'
		WHEN 4 THEN 'BARANG TERKIRIM'
		WHEN 5 THEN 'CANCELLED'
		ELSE '' END)";
	}

	function listMember($status = NULL,$code = NULL) {
		$this->load->model('member_model','Member');
		$opt['select'] = "r.id,r.ae_id,ae.code ae_code, ae.name `ae_name`,r.code,r.gender,r.name,r.address,r.city," . $this->provinceSQL('r.province') . " province,r.zip,r.phone,r.bb_pin,r.email,r.email_2,r.email_3,r.status,ae.address ae_address,ae.city ae_city," . $this->provinceSQL('ae.province') . " ae_province ,ae.zip ae_zip,ae.phone ae_phone,ae.bb_pin ae_bb_pin,ae.email ae_email,ae.email_2 ae_email_2,ae.email_3 ae_email_3";
		$opt['from'] = 'members r';
		$opt['join']['members ae'] = array('r.ae_id = ae.id','left');
		$opt['where']['r.status'] = $status;

		if ($code != NULL) {
			$opt['where']['r.code'] = $code;
		}
		$res = $this->Member->get_list($opt);
		
		$arr['data'] = $res;
		
		
		$arr['rowCount'] = count($res);
		if (count($res) > 0) {
			$arr['colCount'] = count((array) $res[0]);
		} else {
			$arr['colCount'] = 0;
		}


		echo json_encode($arr);
	}	

	function saveMember() {
		if (!$this->input->post('save_member')) return;
		$this->load->model('member_model','Member');
		$id = $this->Member->save($this->input->post());
		if (!$this->input->post('id')){
			$data['id'] = $id;
			$data['code'] = sprintf('%02d', $this->input->post('province')) . sprintf('%04d',$id);
			$data['activation_code'] = md5($data['code']);
			$this->Member->save($data); 	
		} 
		else {
			$data['code'] = $this->input->post('code');
		}

		echo json_encode(array('status' => 'OK','id' => $id, 'code' => $data['code'] ));

	}

	/*function findMemberByCode($code){
		//if (!$this->input->post('find_member')) return;
		$this->load->model('member_model','Member');
		$member = $this->Member->get_by_code($code);

		$status = "NOK"; 
		$data = array();

		if ($member){
			$status = "OK";
			$data = $member;
			if ($member->status == 2) {
				
			}
		}
		echo json_encode(array('status' => $status, 'data' => $data ));

	}*/

	function listPaket() {
		$this->load->model('paket_model','Paket');
		$data = $this->Paket->get_list();
		echo json_encode(array('status' => "OK", 'data' => $data ));

	}

	public function getTotalPaket($paketId, $qty) {
		$this->load->model('paket_model','Paket');
		$paket = $this->Paket->get_by_id($paketId);
		$price = $paket->price;
		$total = $price * $qty;
		$discount = 0;
		if ($paket->disc_qty != NULL ) {
			$qty = floor($qty / $paket->disc_qty );
			if ($paket->multiple == 0 ) {
				if ($qty > 1) $qty = 1;
			}
			$discount = $paket->discount * $qty;
		}
		$total -= $discount;

		echo json_encode(array('total' => $total, 'discount' => $discount));
	}

	public function saveOrder() {
		if (!$this->input->post('save_order')) return;
		$this->load->model('order_model','Order');
		$this->load->model('order_item_model','Item');
		$this->load->model('shipping_model','Shipping');

		$order_data = $this->input->post('order');
		$order_id = $this->Order->save($order_data);
		if (strlen($order_data['code']) == 0) {
			$order_data['id'] = $order_id;
			$order_data['code'] = "PO/" . $order_id . "/" . date('m') . '-' . date('Y') ;
			$this->Order->save($order_data);
		} 

		$this->db->where('order_id', $order_id);
   		$this->db->delete('order_items'); 
		foreach ($this->input->post('item') as $value) {
			$value['order_id'] = $order_id;
			$this->Item->save($value);
		}

		$this->db->where('order_id', $order_id);
   		$this->db->delete('shippings'); 
		foreach ($this->input->post('shipping') as $value) {
			$value['order_id'] = $order_id;
			$this->Shipping->save($value);
		}

		echo json_encode(array('status' => "OK", 'code' => $order_data['code'] ));

	}

	public function listOrder($filter = null){
		$opt = array();
		$opt['select'] = "o.id,o.`code`,o.created_dt,ae.name ae_name,c.code cust_code, c.name cust_name,c.city," . $this->provinceSQL('c.province') . " cust_province,p.name paket_name,i.quantity,i.amount," . $this->saleStatusSQL('o.status') . " sale_status";
		$opt['from'] = "order_items i";
		$opt['join']['orders o'] = array("i.order_id = o.id","left");
		$opt['join']['paket p'] = array("i.paket_id = p.id",'left');
		$opt['join']['members c'] = array('o.member_id = c.id','left');
		$opt['join']['members ae'] = array('c.ae_id = ae.id','left');
		$opt['order_by'] = "o.created_dt desc";
		$filter = urldecode($filter);
		if ($filter != null) $opt['where'][$filter] = null;
		
		$this->load->model('order_model','Order');
		$res = $this->Order->get_list($opt);
		
		$arr['data'] = $res;
		
		$arr['rowCount'] = count($res);
		if (count($res) > 0) {
			$arr['colCount'] = count((array) $res[0]);
		} else {
			$arr['colCount'] = 0;
		}


		echo json_encode($arr);

	}
	
}

