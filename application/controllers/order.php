<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH . 'libraries/REST_Ongkir.php';
    
class Order extends CI_Controller {
	public $_REKENING = array('BCA-WarungBuncit No. 552 038 9204','Bank Mandiri-Jatipadang No. 127 000 647 2912') ;	

	public function index()
	{
		$member = isLogged();
		
		if (!$member) {
			redirect(base_url('member/noAccess'));
		}

		$this->load->view('front/header',array('menu' => 'order' ));
		$this->load->model('paket_model','Paket');

		$shipping_count = 1;
		if ($this->input->post('order')) {
			$POST = $this->input->post();
			$shipping_count = $POST['shipping_count'];
			$total_amount = 0;
			$total_quantity = 0;
			$total_shipping = 0;
			$item = array();
			foreach ($POST['quantity'] as $key => $value) {
				if ($value > 0){
					$paket = $this->Paket->get_by_id($key);
					$amount = str_replace(',', '', $POST['amount'][$key]) ;
					$total_amount += $amount;
					$item[] = array('paket_id' => $paket->id, 'quantity' => $value, 'amount' => $amount);
					$total_quantity += $value;
				}
			}

			if(count($item) > 0 ) {
				$_POST['paket'] = 'ok';
			}

			$this->load->library(array('form_validation'));
			$this->form_validation->set_error_delimiters('<div class="error_note">', '</div>');
			$this->form_validation->set_message('required', '* %s harus diisi');
			$this->form_validation->set_message('is_natural_no_zero', '* %s harus lebih dari 0');

			$this->form_validation->set_rules('paket', 'Salah satu paket', 'required|xss_clean');
			$this->form_validation->set_rules('quantity' , 'quantity', 'xss_clean');
			$this->form_validation->set_rules('have_read', '', 'required|xss_clean');
			

			foreach ($POST['shipping_number'] as $value) {
				$this->form_validation->set_rules('shipping_type_' . $value , '', 'xss_clean');
				if ($this->input->post('shipping_type_' . $value ) == 'courier' ) {
					$this->form_validation->set_rules('shipping_receiver_' . $value, 'Penerima', 'required|xss_clean');
					$this->form_validation->set_rules('shipping_address_' . $value, 'Alamat', 'required|xss_clean');
					$this->form_validation->set_rules('shipping_city_' . $value, 'Kab/Kota', 'required|xss_clean');
					$this->form_validation->set_rules('shipping_province_' . $value, 'Provinsi', 'required|xss_clean');
					$this->form_validation->set_rules('shipping_zip_' . $value, 'Kode Pos', 'required|xss_clean');
					$this->form_validation->set_rules('shipping_phone_' . $value, 'Telp/HP', 'required|xss_clean');
				}
				$this->form_validation->set_rules('shipping_qty_' . $value, 'Jumlah Kirim', 'required|is_natural_no_zero|xss_clean');
				$total_shipping += $this->input->post('shipping_qty_' . $value);
			}

			if( $total_quantity == $total_shipping ) {
				$_POST['shipping'] = 'ok';
			}

			$this->form_validation->set_rules('shipping' , 'shipping ', 'required|xss_clean');

			if($this->form_validation->run()) {
				$this->load->model('order_model','Order');
				$this->load->model('order_item_model','Item');
				$this->load->model('shipping_model','Shipping');

				$POST['member_id'] = $member->id;
				$POST['total_amount'] = $total_amount;

				$orderId = $this->Order->save($POST);

				/*$POST['code'] =  $member->code . '-'  . $orderId  ;
				$POST['id'] = $orderId;
				$this->Order->save($POST);*/

				foreach ($item as $value) {
					$value['order_id'] = $orderId;
					$this->Item->save($value);
				}

				$do_number = 0;
				$total_cost = 0;
				foreach ($POST['shipping_number'] as $value) {
					$do_number++;
					$shipping = array( 'order_id' => $orderId, 'do_number' => $do_number, 'status' => 0 );
					$shipping['shipping_receiver']= $this->input->post('shipping_receiver_' . $value);
					$shipping['shipping_address']= $this->input->post('shipping_address_' . $value);
					$shipping['shipping_city']= $this->input->post('shipping_city_' . $value);
					$shipping['shipping_province']= $this->input->post('shipping_province_' . $value);
					$shipping['shipping_zip']= $this->input->post('shipping_zip_' . $value);
					$shipping['shipping_phone']= $this->input->post('shipping_phone_' . $value);
					$shipping['shipping_qty']= $this->input->post('shipping_qty_' . $value);
					$cost = str_replace(',', '', $this->input->post('shipping_cost_' . $value));
					$shipping['shipping_cost'] = $cost;
					$total_cost += $cost;
					$this->Shipping->save($shipping);
				}
				$this->Order->save(array('id' => $orderId, 'code' =>  $member->code . '-'  . $orderId, 'total_cost' => $total_cost, 'total_price' => $total_amount + $total_cost ));
				$data['code'] = $member->code . '-'  . $orderId	;
				$this->load->view('front/order_finish',$data);
				return;
			} 

			$data['post'] = true;
		} else {
			$data['post'] = false;
		}

		$data['member'] = $member;
		$this->load->helper('form');
		$data['paketColl'] = $this->Paket->get_list();
		
		for ($i=1; $i <= $shipping_count ; $i++) { 
			$data['shipping_number'] = $i ;
			$shipping_form[] = $this->load->view('front/order_shipping',$data,TRUE);
		}
		$data['shipping_form'] = $shipping_form;
		$data['shipping_count'] = $shipping_count;
		$this->load->view('front/order',$data);

		$this->load->view('front/footer');
	
	}

	public function getTotalPaket($paketId, $qty) {
		$this->load->model('paket_model','Paket');
		$paket = $this->Paket->get_by_id($paketId);
		$price = $paket->price;
		$total = $price * $qty;
		$discount = 0;
		if ($paket->disc_qty != NULL ) {
			$qty = floor($qty / $paket->disc_qty );
			if ($paket->multiple == 0 ) {
				if ($qty > 1) $qty = 1;
			}
			$discount = $paket->discount * $qty;
		}
		$total -= $discount;

		//echo "{'discount' : '" . $discount . "','total' :'" . $total . "'}"  ;
		echo json_encode(array('total' => $total, 'discount' => $discount));
	}

	public function orderHistory() {
		$member = isLogged();
		
		if (!$member) {
			redirect(base_url('member/noAccess'));
		}
		$this->load->model('order_model','Order');
		$data['order'] = $this->Order->get_by_member($member->id);

		$this->load->view('front/header');		
		$this->load->view('front/order_history',$data);
		$this->load->view('front/footer');

	}

	

	public function orderHistoryShipping($order_id) {
		$member = isLogged();
		
		if (!$member) {
			redirect(base_url('member/noAccess'));
		}

		$this->load->model('order_model','Order');
		$this->load->model('shipping_model','Shipping');
		$data['order'] = $this->Order->get_detail_by_id($order_id);
		$data['shipping'] = $this->Shipping->get_by_order($order_id);

		$this->load->view('front/order_history_shipping',$data);

	}

	public function orderHistoryDetail($order_id) {
		$member = isLogged();
		
		if (!$member) {
			redirect(base_url('member/noAccess'));
		}

		$this->load->helper('form');
		$this->load->model('order_model','Order');
		$this->load->model('payment_model','Payment');
		if ($this->input->post('submit_confirm')) {
			$_POST['transfer_date'] = $_POST['transfer_date'][0] .'-' . $_POST['transfer_date'][1] . '-' . $_POST['transfer_date'][2];
			$_POST['transfer_amount'] = str_replace(',', '', $_POST['transfer_amount']);
			$post = $this->input->post();

			$this->load->library(array('form_validation'));
			$this->form_validation->set_error_delimiters('<div class="error_note">', '</div>');
			$this->form_validation->set_message('required', '* %s harus diisi');
			$this->form_validation->set_message('numeric', '* %s hanya diisi dengan angka');
			$this->form_validation->set_message('matches', '* %s harus sama dengan total tagihan');

			$this->form_validation->set_rules('transfer_date', '', 'xss_clean');
			$this->form_validation->set_rules('account_number', 'Nomor Rekening', 'required|xss_clean');
			$this->form_validation->set_rules('account_bank', 'Bank', 'required|xss_clean');
			$this->form_validation->set_rules('account_name', 'Nama Pemilik Rekening', 'required|xss_clean');
			$this->form_validation->set_rules('destination_account', 'Rekening Bank Tujuan', 'required|xss_clean');
			$this->form_validation->set_rules('transfer_amount', 'Jumlah Transfer', 'numeric|required|xss_clean|matches[total_price]');

			if($this->form_validation->run()) {
				$post['order_id'] = $order_id;
				$post['confirm_date'] = time();

				$opt['where']['order_id'] = $order_id;
				$payment = $this->Payment->get_list($opt);
				if (count($payment) > 0) {
					$post['id'] = $payment[0]->id;
				}
				$this->Payment->save($post);

				$opt = array('id' => $order_id, 'status' => 1 );
				$this->Order->save($opt);
			}

			$data['post'] = true;

		} else {
			$data['post'] = false;

		}



		$data['_REKENING'] = $this->_REKENING;
		$data['order'] = $this->Order->get_detail_by_id($order_id);

		$this->load->view('front/order_history_detail',$data);

	}

	function addShippingAddress($currentNumber) {
		$member = isLogged();
		$data['member'] = $member;
		$this->load->helper('form');
		$data['shipping_number'] = $currentNumber ;
		$data['post'] = false;
		$shipping_form[] = $this->load->view('front/order_shipping',$data);
	}

	function getCities($province){
		echo json_encode(getCities($province));
	}

	

	function getShippingCost($city) {
		$rest = new REST_Ongkir(array(
	        'server' => 'http://api.ongkir.info/'
	    ));
    
    	$result = $rest->post('cost/find', array(
	        'from' 	=> 'JAKARTA', 
	        'to' 		=> $city,
	        'weight'	=> 1000, 
	        'courier'	=> 'jne',
	        'API-Key' 	=> 'd1fc4c4928a143302f46478e517fbdb1',
	        'format'	=> 'json'
	    ));
	    try
	    {
	        $status = $result->status;
        	if ($status->code == 0){
            	foreach ($result->price as $value ) {
            		if ($value->service_code == 'reg' ) {
						echo ' { "status" : "OK", "cost" : "' . $value->value . '", "city" : "' . $result->city->destination . '" }';
						return;
            		}
            	}
        	}
        	else {
        	}
        
    	}
		catch (Exception $e) {
        
    	}
		echo ' { "status" : "NOK" }';

	}


}