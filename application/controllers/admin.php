<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('url','admin_helper'));
	}

	public function isAdmin() {
		$this->load->library('session');    
		$admin = $this->session->userdata('admin');
        if ($admin)  {
        	return true;
        } else {
			$this->load->helper(array('url'));
			redirect('admin/login', 'refresh');

        }
	}

	public function index() {
		if ($this->isAdmin()) {
			$this->load->view('admin/header');
			//$this->load->view('admin/footer');
		} else {
			echo 'NO';
		}
	}

	public function login() {
		$tpl = array();
		if ($this->input->post('login')) {
			$CI =& get_instance();
			$admin_user_name = $CI->config->item('admin_user_name') ;
			$admin_user_pass = $CI->config->item('admin_user_pass') ;

			if ( $this->input->post('user_name') == $admin_user_name && $this->input->post('user_pass') == $admin_user_pass )
			{
				$this->load->library('session');
				$login['admin']['name'] = $admin_user_name ; 
				$this->session->set_userdata($login);
				
				redirect('admin', 'refresh');
				return;
			} else {
				$tpl['status'] = LOGIN_FAILED;
			}

		} 
		
		$this->load->view('admin/login',array('tpl' => $tpl));		
	
		
	}

	public function logout()
	{
		$this->load->library('session');
		$this->session->unset_userdata('admin');
		$this->login();
	}

	public function delete_record($table, $id) {
		if ($this->isAdmin()) {
			if ($table == 'training') {
				$this->load->model('training_model','Training');

				$data = array();
				$data['id'] = $id;
				$data['active'] = 0;
				$this->Training->save($data);
				echo '1';
				return;
			}
			$this->db->where('id',$id);
			$res =	$this->db->delete($table);
			echo $res;
		}
		return;
	}
}

