<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member extends CI_Controller {

	public function index() {

	}

	

	public function register()
	{
		
		$this->load->view('front/header',array('menu' => 'register' ));
		$this->load->helper('form');
		$register_ok = false;
		if ($this->input->post('register')) {
			$this->load->library(array('form_validation'));
			$this->form_validation->set_error_delimiters('<div class="error_note">', '</div>');
			$this->form_validation->set_message('required', '* %s harus diisi');

			$this->form_validation->set_rules('ae_id', 'Kode AE', 'required|xss_clean');
			$this->form_validation->set_rules('name', 'Nama Lengkap', 'required|xss_clean');
			$this->form_validation->set_rules('address', 'Alamat', 'required|xss_clean');
			$this->form_validation->set_rules('city', 'Kab/Kota', 'required|xss_clean');
			$this->form_validation->set_rules('province', 'Provinsi', 'required|xss_clean');
			$this->form_validation->set_rules('zip', 'Kode Pos', 'required|xss_clean');
			$this->form_validation->set_rules('phone', 'Telp/HP', 'required|xss_clean');
			$this->form_validation->set_rules('bb_pin', '', 'xss_clean');
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email|xss_clean');
			$this->form_validation->set_rules('email_2', 'Email', 'xss_clean');
			$this->form_validation->set_rules('email_3', 'Email', 'xss_clean');
			$this->form_validation->set_rules('password', 'Password', 'required|xss_clean');
			$this->form_validation->set_rules('have_read', '', 'required|xss_clean');

			if($this->form_validation->run()) {
				$this->load->model('member_model','Member');
		        $this->load->library(array('parser','email'));

				$data = $this->input->post();
				$id = $this->Member->save($data);
				$data['id'] = $id;
				$data['code'] = sprintf('%02d', $data['province'])   . sprintf('%04d',$id);
				$data['activation_code'] = md5($data['code']);
				$this->Member->save($data);

				$data['activation_link'] = base_url('member/activation') . '/' . $data['code'] . '/' . $data['activation_code'];
				$body = $this->load->view('email/registration',array(),true);
		        $body = $this->parser->parse_string($body, $data,true); 


				$this->email->from = array( ADMIN_EMAIL => ADMIN_TITLE);
				$this->email->recipient = array($data['email'] => $data['email']);
				$this->email->subject = "REGISTRASI RESELLER";
				$this->email->body = $body ;
				try {
					$sent = $this->email->send();
		        } catch (Exception $e) {
		        	show_error($e->getMessage());
		        }

				
				$message['msg_title'] = "Terima kasih..";
				$message['msg_body'] = "Permintaan registrasi anda sudah terkirim. Silakan cek email anda, dan klik url aktivasi untuk melengkapi pendaftaran";
				$this->load->view('front/message',$message);
				$register_ok = true;
			} 

		} 
		if (!$register_ok) $this->load->view('front/register');

		$this->load->view('front/footer');
	}

	public function order() {
		$member = isLogged(true);

	}

	public function activation($code,$activation_code) {
		$this->load->model('member_model','Member');
		$member = $this->Member->get_by_code($code);		
		if ($member) {
			if ($member->activation_code == $activation_code ) {
				$data['id'] = $member->id;
				$data['status'] = 1;
				$this->Member->save($data);
				$message['msg_title'] = "Terima kasih..";
				$message['msg_body'] = 'Pendaftaran telah lengkap, silakan login menggunakan akun anda<br />kembali ke <a href="' . base_url() . '">halaman muka</a>';

			} else {
				$message['msg_title'] = "Maaf..";
				$message['msg_body'] = "URL aktivasi tidak valid, silakan cek kembali email anda";				
			}

		} else {
			$message['msg_title'] = "Maaf..";
			$message['msg_body'] = "URL aktivasi tidak valid, silakan cek kembali email anda";				
		}
		$this->load->view('front/message',$message);

	}

	public function login() {

		$this->load->model('member_model','Member');
		$member = $this->Member->get_by_email($this->input->post('email'));		
		if ($member) {
			if ($member->password == $this->input->post('password')) {
				$this->load->library('session');
				$this->session->unset_userdata('user');
				
				$this->session->set_userdata(array('user' => $member));
				redirect( base_url() ,'refresh');
			}
		}
		$message['msg_title'] = "Maaf..";
		$message['msg_body'] = 'email dan/atau password anda salah<br />kembali ke <a href="' . base_url() . '">halaman muka</a>';
		$this->load->view('front/message',$message);

	}

	public function logout()
	{
		$this->load->library('session');
		$this->session->unset_userdata('user');
		$message['msg_title'] = "Logout berhasil...";
		$message['msg_body'] = 'kembali ke <a href="' . base_url() . '">halaman muka</a>';
		$this->load->view('front/message',$message);
	}

	public function noAccess() {

		$message['msg_title'] = "Maaf..";
		$message['msg_body'] = 'Anda harus login sebagai member untuk mengakses halaman ini<br />kembali ke <a href="' . base_url() . '">halaman muka</a>';
		$this->load->view('front/message',$message);
	}

	public function forgetPassword(){
		$data = array();
		if ($this->input->post('forget_password')){
			if ($this->input->post('email') == NULL ) {
				$data['error'] = "* Anda belum mengisi email";
			} else {
				$email = $this->input->post('email');
				$this->load->model('member_model','Member');
				$member = $this->Member->get_by_email($email);
				if (!$member) {
					$data['error'] = "* Email tidak ditemukan";
				} else {
			        $this->load->library(array('parser','email'));
			        $data['name'] = $member->name;
			        $data['email'] = $member->email;
			        $data['password'] = $member->password;
			        $body = $this->load->view('email/forget_password',array(),true);
			        $body = $this->parser->parse_string($body, $data,true);

					$this->email->from = array( ADMIN_EMAIL => ADMIN_TITLE);
					$this->email->recipient = array($member->email => $member->email);
					$this->email->subject = "PASSWORD RESELLER";
					$this->email->body = $body ;
					try {
						$sent = $this->email->send();
			        } catch (Exception $e) {
			        	show_error($e->getMessage());
			        }

					$message['msg_title'] = "Terima kasih";
					$message['msg_body'] = 'Password telah terkirim ke email anda<br/>kembali ke <a href="' . base_url() . '">halaman muka</a>';
					$this->load->view('front/message',$message);
					return;
				}

			}

		} 
		$this->load->helper('form');
		$this->load->view('front/header');
		$this->load->view('front/forget_password',$data);
		$this->load->view('front/footer');

	}

}
