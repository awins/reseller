<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . 'models/ci_model_mod.php');

class Payment_model extends CI_Model_Mod {

    function __construct()
    {
        parent::__construct();

        $this->table = 'payments';
        $this->scheme = array(
            'id',
            'order_id',
            'confirm_date',
            'checked_date', 
            'transfer_date',
            'account_name',
            'account_number',
            'account_bank',
            'destination_account',
            'transfer_amount'
        );
    }


}

?>