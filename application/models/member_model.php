<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . 'models/ci_model_mod.php');

class Member_model extends CI_Model_Mod {

    function __construct()
    {
        parent::__construct();

        $this->table = 'members';
        $this->scheme = array(
            'id',
            'code',
            'name',
            'address',
            'city',
            'province',
            'zip',
            'phone',
            'bb_pin',
            'email',
            'email_2',
            'email_3',
            'activation_code',
            'password',
            'status'
        );
    }
    
    
    public function get_by_code($code) {
        $opt['where']['code'] = $code;
        $result = $this->get_list($opt);
        if (count($result) > 0 ) {
            return $result[0];
        } else {
            return false;
        }

    }

    public function get_by_email($email) {
        $opt['where']['email'] = $email;
        $opt['where']['status'] = 1;
        $result = $this->get_list($opt);
        if (count($result) > 0 ) {
            return $result[0];
        } else {
            return false;
        }

    }

}

?>