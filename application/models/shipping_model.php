<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . 'models/ci_model_mod.php');

class Shipping_model extends CI_Model_Mod {

    function __construct()
    {
        parent::__construct();

        $this->table = 'shippings';
        $this->scheme = array(
            'id',
            'order_id',
            'do_number',
            'shipping_cost',
            'shipping_receiver',
            'shipping_address',
            'shipping_city',
            'shipping_province',
            'shipping_zip',
            'shipping_phone',
            'shipping_qty',
            'delivery_dt',
            'status',
            'awb_number',
            'notes'
        );
    }
    
    function get_by_order($order_id){
        $opt = array();
        $opt['where']['order_id'] = $order_id;
        $opt['order_by'] = 'do_number';
        return $this->get_list($opt);
    }
}

?>