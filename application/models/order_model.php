<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . 'models/ci_model_mod.php');

class Order_model extends CI_Model_Mod {

    function __construct()
    {
        parent::__construct();

        $this->table = 'orders';
        $this->scheme = array(
            'id',
            'code',
            'member_id',
            'total_amount',
            'total_cost', 
            'total_price',
            'notes',
            'status',
            'created_dt',
            'payment_dt',
        );
    }
    
    function get_by_member($member_id) {
        $opt['where']['member_id'] = $member_id;
        $opt['order_by'] = 'created_dt desc';
        return $this->get_list($opt);
    }

    function get_detail_by_id($order_id) {
        $opt['select'] = "o.*, d.paket_id,d.quantity,d.amount, p.name, p.short_description, p.price,
                        py.transfer_date,py.account_number, py.account_bank, py.account_name, py.destination_account, py.transfer_amount ";
        $opt['from'] = "orders o";
        $opt['join']['order_items d'] = array("d.order_id = o.id","left");
        $opt['join']['paket p'] = array("d.paket_id = p.id","left");
        $opt['join']['payments py'] = array("py.order_id = o.id","left");


        $opt['where']['o.id'] = $order_id;        
        return $this->get_list($opt);
    }
}

?>