<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');


/* End of file constants.php */
/* Location: ./application/config/constants.php */

// EMAIL CONFUGURATION

define("SMTP_SERVER", "smtp.gmail.com");
define("SMTP_USERNAME", "awin.email.system@gmail.com");
define("SMTP_PASSWORD", "emailSystem");
define("SMTP_PORT", 465);
define("SMTP_METHOD", "ssl");


/*define("SMTP_SERVER", "mail.pickenchoose.com");
define("SMTP_USERNAME", "cs@pickenchoose.com");
define("SMTP_PASSWORD", "P@2BU-bG6xtq");
define("SMTP_PORT", 25);
define("SMTP_METHOD", "ssl");*/

define("ADMIN_EMAIL", "reseller@pustaka-lebah.com");
define("ADMIN_TITLE", "RESELLER PL");