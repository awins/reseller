(function ($) {
	
	/* jquery slide by adul */
	
	$(document).ready(function(){
  		$("#btn-des-reseller").click(function(){
    		$("#deskripsi-reseller").show();
			$("#manfaat-reseller").hide();
			$("#cara-reseller").hide();
  		});
		
		$("#btn-manfaat-reseller").click(function(){
    		$("#manfaat-reseller").slideDown(2000);
			$("#deskripsi-reseller").hide();
			$("#cara-reseller").hide();
  		});
		
		$("#btn-cara-reseller").click(function(){
    		$("#cara-reseller").slideDown(2000);
			$("#manfaat-reseller").hide();
			$("#deskripsi-reseller").hide();
  		});
	});
	
	/* end jquery slide by adul */
	
	
	

	Number.prototype.formatMoney = function(c, d, t){
		var n = this, 
		    c = isNaN(c = Math.abs(c)) ? 2 : c, 
		    d = d == undefined ? "." : d, 
		    t = t == undefined ? "," : t, 
		    s = n < 0 ? "-" : "", 
		    i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
		    j = (j = i.length) > 3 ? j % 3 : 0;
	   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
	 };

	$('.input-number').bind('keypress', function(event) {
		var theEvent = event || window.event;
		var key = theEvent.keyCode || theEvent.which;
		key = String.fromCharCode( key );
		var regex = /[0-9]|\./;
		if( !regex.test(key) ) {
		    theEvent.returnValue = false;
		    if(theEvent.preventDefault) theEvent.preventDefault();
		 }
	});

	$('.input-number').bind('change keyup', function() {
		var qty = Number($(this).val());
		if (qty < 0) {
		 	$(this).val(0);
		} 
	});
	
	$('.quantity').bind('change keyup', function() {
		var qty = Number($(this).val());
		if (qty < 2) qty = 2;
		if (qty == 6) qty = 7;

	 	$(this).val(qty);

		var id = $(this).attr('id').replace('quantity_','');

		var url = "http://"+window.location.hostname + "/order/getTotalPaket/" +  id + "/" + qty ;

		$.get(url).done(function(data) {
			console.log(data);
			var result = jQuery.parseJSON(data);
			var discount = parseFloat( result.discount);
            var amount = parseFloat(result.total);
            $('#discount_' + id ).val(discount.formatMoney(0, '.', ','));
            $('#amount_' + id ).val(amount.formatMoney(0, '.', ','));
			
			var total_amount = 0;

			$('.amount').each(function() {
				total_amount += Number($(this).val().replace(/\,/g,''));
			});

            $('#total_amount').html(total_amount.formatMoney(0, '.', ','));
        });
	});	

	$('#table_order tbody tr').click(function() {
		$('#table_order tbody tr').removeClass('selected');
		$(this).addClass('selected');
		$('#history-nav ul li').css('display','block');
		$('#history-nav ul li[rel="detail"]').click();
	});

	function btnConfirmClick(orderID) {
		var url = "http://"+window.location.hostname + '/order/orderHistoryDetail/' + orderID; 
		$('#submit_confirm').click(function() {
			console.log('submit click');
			$('#form_confirmation').ajaxSubmit({
				url: url,
				type: 'post',
				success: function(data) {
					$('#history-box').html(data);
					btnConfirmClick(orderID);
					inputAmountListener();
				}		
			});
		});
	}

	function inputAmountListener(){
		$('#transfer_amount').on('input',function(){
			var val = parseFloat( $(this).val());
			$('#transfer_amount_formated').html(val.formatMoney('0', '.',','));
		});

		$('#transfer_amount').focusin(function(){
			$(this).val($(this).val().replace(/\,/g,'') );
		});
		$('#transfer_amount').focusout(function(){
			var val = parseFloat( $(this).val());
			$(this).val(val.formatMoney('0', '.',','));
		});
	}

	$('#history-nav ul li').click(function() {
		var menu =	$(this).attr('rel');
		$('#history-nav ul li').removeClass('selected');
		$(this).addClass('selected');

		switch(menu) {
			case 'list':
				$('#history-list').css('display','block');
				$('#history-box').css('display','none');
				break;
			case 'detail':
				var tr = $('#table_order tbody tr.selected') ;
				var id = $(tr[0]).attr('rel');
				var url = "http://"+window.location.hostname + '/order/orderHistoryDetail/' + id; 
				$.get(url).done(function(data) {
					$('#history-list').css('display','none');
					$('#history-box').css('display','block');
					$('#history-box').html(data);
					btnConfirmClick(id);
					inputAmountListener();
				});
				break
			case 'shipping':
				var tr = $('#table_order tbody tr.selected') ;
				var id = $(tr[0]).attr('rel');
				var url = "http://"+window.location.hostname + '/order/orderHistoryShipping/' + id; 
				$.get(url).done(function(data) {
					$('#history-list').css('display','none');
					$('#history-box').css('display','block');
					$('#history-box').html(data);
				});
				break;
			default:
				break;
		}


	});

	$('#btnAddShipping').click(function() {
		var count = $('#shipping_count').val();
		count++;
		var url = "http://"+window.location.hostname + '/order/addShippingAddress/' + count; 
		var html = $('#shipping_box').html();
		$.get( url ).done(function(data) {
			html += data;
			$('#shipping_box').append(data);
			$('#shipping_count').val(count);
			ShippingListener();
		});
	});

	function calcTotal() {
		var total = 0;
		$('.shipping_cost').each(function(){
			total += Number($(this).val().replace(/\,/g,''));
		});
		$('#total_cost').html(parseFloat(total).formatMoney(0, '.', ','));
		total +=  Number($('#total_amount').html().replace(/\,/g,''));
		$('#total_price').html(parseFloat(total).formatMoney(0, '.', ','));

	}


	function calcElmCost(id) {
		var qty = $('#shipping_qty_' + id).val();
		var selCity = $("#shipping_city_" + id + " option:selected");

		var cost = parseFloat(selCity.attr('cost')) * parseFloat(qty);
		console.log(selCity.attr('cost'));
		var costElm = $('#shipping_cost_' + id);
		costElm.val(parseFloat( cost).formatMoney(0, '.', ','));
	}

	function ShippingListener(){
		$('.btnDelShipping').click(function(){
			var rel = $(this).attr('rel');
			$('#shipping_box_' + rel).remove();
		});

		$('.shipping_province').bind('change keyup', function(){
			var prov = $("option:selected", this).val();
			var url = "http://"+window.location.hostname + "/order/getCities/" +  prov ;
			var rel = $(this).attr('rel');
			var citiElm = $('#shipping_city_' + rel);
			$.get(url).done(function(data) {
				citiElm.empty();
				data = $.parseJSON(data);
				$.each(data, function(index, value){
                    citiElm.append('<option value="'+value+'">'+value+'</option>') ;
				});
                citiElm.prepend('<option value="" cost="0">--PILIH--</option>');
                citiElm.val($("option:first", citiElm).val());
                citiElm.change();

			});
		});

		$('.shipping_city').bind('change keyup', function(){
			var id = $(this).attr('id').replace('shipping_city_','') ;
			var sel = $("option:selected", this);
			var loader = $('#loader_' + id);
			if (!sel.attr('cost') ) {
				loader.show();
				var url = "http://"+window.location.hostname + "/order/getShippingCost/" +  sel.val() ;
				$.get(url).done(function(data) {
					data = $.parseJSON(data);
					if (data.status == "OK") {
						sel.attr('cost',data.cost);
					} else {
						sel.attr('cost',0);
					}

					calcElmCost(id);
					calcTotal();
					loader.hide();
				});			
			} else {
				calcElmCost(id);
				calcTotal();
			}
		});

		$('.shipping_qty').bind('change keyup', function(){
			var id = $(this).attr('id').replace('shipping_qty_','') ;
			calcElmCost(id);
			calcTotal();
		});

		$('.shipping_type').change(function(){
			var rel = $(this).attr('rel');
			var elm = $("input[name='shipping_type_" + rel + "']:checked").val()  ;
			console.log(elm);
			if (elm == 'courier') {
				$('#shipping_courier_' + rel).show();
			} else {
				$('#shipping_courier_' + rel).hide();
			}
		});
	}

	$('.lbl_disclaimer').click(function(){
		var rel = $(this).attr('rel');
		$('#disclaimer-' + rel).dialog({
			height: 400,
			width: 800,
			modal: true
		});
	});

	$(document).ready(function() {
		console.log('ready11');
		$('.quantity').each(function(){
			$(this).change();
		});
		ShippingListener();
	});

})(jQuery);